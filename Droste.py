import numpy
import cv2
import math



def MakeComplexMatrix(firstMatrix, secondMatrix):
	result = []
	for i in range(len(firstMatrix)):
		result.append([])
		for j in range(len(firstMatrix[i])):
			result[-1].append(complex(firstMatrix[i][j], secondMatrix[i][j]))
	return result


def MakeMatricesMappedLn(matrix, r1, r2):
	Wx = []
	Wy = []
	xMax = 0
	yMax = 0
	for i in range(len(matrix)):
		Wx.append([])
		Wy.append([])
		for j in range(len(matrix[i])):
			a, b = WLn(matrix[i][j], r1, r2)
			Wx[-1].append(a)
			Wy[-1].append(b)
			if(xMax < abs(a)):
				xMax = abs(a)
			if(yMax < abs(b)):
				yMax = abs(b)
	return Wx, Wy, xMax, yMax


def MakeMatricesMappedT(XM, YM, xMax, yMax, row, column):
	newXM = []
	newYM = []
	for i in range(len(XM)):
		newXM.append([])
		for j in range(len(XM[i])):
			newXM[-1].append(math.floor((XM[i][j] / xMax + 1) * column / 2))

	for i in range(len(YM)):
		newYM.append([])
		for j in range(len(YM[i])):
			newYM[-1].append(math.floor((YM[i][j] / yMax + 1) * row / 2))
	return newXM, newYM


def WLn(z, r1, r2):
	if(r1 ** 2 <= z.real ** 2 + z.imag ** 2 <= r2 ** 2):
		w1 = complex(z.real / r1, z.imag / r1)
		u = math.log(w1.real ** 2 + w1.imag ** 2) / 2
		s = 0
		if(w1.real == 0):
			if(w1.imag > 0):
				s = math.inf
			else:
				s = math.inf * (-1)
		else:
			s = w1.imag / w1.real
		v = math.atan(s)
		if(w1.real < 0):
			if(w1.imag >= 0):
				v += math.pi
			else:
				v -= math.pi
		return u, v
	return 0, 0


def MakeWLnMappedMatrix(mainMatrix, mappedMatrixX, mappedMatrixY):
	newMatrix = mainMatrix.copy()
	for i in range(row):
		for j in range(column):
			for k in range(3):
				newMatrix[i][j][k] = 0
	for i in range(len(mainMatrix)):
		for j in range(len(mainMatrix[i])):
				newMatrix[(mappedMatrixY[i][j])-1][mappedMatrixX[i][j]-1] = mainMatrix[i][j].copy()
	return newMatrix


def MakeMatricesMappedRotate(matrix, teta, row, column):
	Wx = []
	Wy = []
	for i in range(row):
		Wx.append([])
		Wy.append([])
		for j in range(column):
			zTemp = Rotate(matrix[i][j], teta)
			Wx[-1].append(zTemp.real)
			Wy[-1].append(zTemp.imag)
	return Wx, Wy


def MakeMatricesMappedR(Xmatrix, Ymatrix):
	XM = []
	YM = []
	for i in range(row):
		XM.append([])
		YM.append([])
		for j in range(column):
			XM[-1].append(math.floor((Xmatrix[i][j] + (2 ** 0.5)) * row / (8 ** 0.5)))
			YM[-1].append(math.floor((Ymatrix[i][j] + (2 ** 0.5)) * column / (8 ** 0.5)))
	return XM, YM


def MakeWRotateMappedMatrix(mainMatrix, XM, YM):
	image3 = mainMatrix.copy()
	for i in range(len(image3)):
		for j in range(len(image3[i])):
			for k in range(3):
				image3[i][j][k] = 0
	for i in range(row):
		for j in range(column):
			if (0 <= XM[i][j] < row and 0 <= YM[i][j] < column):
				image3[YM[i][j]][XM[i][j]] = mainMatrix[i][j]
	return image3


def Rotate(z, teta):
	mainTeta = 0
	if(z.real != 0):
		mainTeta = math.atan(z.imag / z.real)
	else:
		if(z.imag >= 0):
			mainTeta = math.pi / 2
		else:
			mainTeta = math.pi / (-2)
	if z.real < 0:
		if(z.imag >= 0):
			mainTeta += math.pi
		else:
			mainTeta -= math.pi
	newTeta = mainTeta - teta
	r = (z.real ** 2 + z.imag ** 2) ** 0.5
	return complex(r * math.cos(newTeta), r * math.sin(newTeta))


def RewritePicture(image, n, WxLn, WyLn, xMaxLn):
	newImage = []
	WxExp = []
	WyExp = []
	for i in range(n):
		for j in range(len(image)):
			WxExp.append([])
			WyExp.append([])
			for k in range(len(image[j])):
				WyExp[-1].append(WyLn[j][k])
				WxExp[-1].append(WxLn[j][k])
				WxExp[-1][-1] -= i * xMaxLn
			newImage.append(image[j])
	return numpy.tile(newImage, 1), WxExp, WyExp


def MakeCoherenceW(Wx, Wy, coherence):
	WxExp = []
	WyExp = []
	for i in range(len(Wx)):
		WxExp.append([])
		WyExp.append([])
		for j in range(len(Wx[i])):
			WxExp[-1].append(coherence * Wx[i][j])
			WyExp[-1].append(coherence * Wy[i][j])
	return WxExp, WyExp


def WExp(x, y):
	return (math.e ** x) * complex(math.cos(y), math.sin(y))


def MakeMatricesMappedExp(Xmatrix, Ymatrix):
	X = []
	Y = []
	xminR = 0
	xmaxR = 0
	yminR = 0
	ymaxR = 0
	for i in range(len(Xmatrix)):
		X.append([])
		Y.append([])
		for j in range(len(Xmatrix[i])):
			z = WExp(Xmatrix[i][j], Ymatrix[i][j])
			X[-1].append(z.real)
			Y[-1].append(z.imag)
			if(xminR > z.real):
				xminR = z.real
			if(xmaxR < z.real):
				xmaxR = z.real			
			if(yminR > z.imag):
				yminR = z.imag
			if(ymaxR < z.imag):
				ymaxR = z.imag
	return X, Y, xminR, xmaxR, yminR, ymaxR

		
def MakeMatriciesMappedS(Wx, Wy, xRange, xbase, row, yRange, yBase, column):
	X = []
	Y = []
	for i in range(len(Wx)):
		X.append([])
		Y.append([])
		for j in range(len(Wx[i])):
			X[-1].append(math.floor((Wx[i][j] + xbase) / xRange * row) )
			Y[-1].append(math.floor((Wy[i][j] + yBase) / yRange * column) )
	return X, Y


def MakeWExpMappedMatrix(mainImage, repeatedImage, Wx, Wy, row, column):
	newImage = mainImage.copy()
	for i in range(len(newImage)):
		for j in range(len(newImage[i])):
			for k in range(3):
				newImage[i][j][k] = 0

	for i in range(len(Wx)):
		for j in range(len(Wx[i])):
			if( 0 <= Wy[i][j] < column and 0 <= Wx[i][j] < row):
				newImage[Wy[i][j]][Wx[i][j]] = mainImage[i%row][j%column]
	return newImage


# Part 0
image1 = cv2.imread('clock.jpg', 1)
row = len(image1)
column = len(image1[0])
xtemp = numpy.linspace(-1, 1, row)
ytemp = numpy.linspace(-1, 1, column)
X, Y = numpy.meshgrid(xtemp, ytemp)
zMatrix = MakeComplexMatrix(X, Y)

# Part 1
r1 = 0.17
r2 = 1
WxLn, WyLn, XMaxLn, YMaxLn = MakeMatricesMappedLn(zMatrix, r1, r2)
XMLn, YMLn = MakeMatricesMappedT(WxLn, WyLn, XMaxLn, YMaxLn, row, column)
image2 = MakeWLnMappedMatrix(image1, XMLn, YMLn)
cv2.imwrite('LN.jpg', image2)


# Part 2
WxRotate, WyRotate = MakeMatricesMappedRotate(zMatrix, -math.pi / 4, row, column)
XMR, YMR = MakeMatricesMappedR(WxRotate, WyRotate)
image3 = MakeWRotateMappedMatrix(image1, XMR, YMR)
cv2.imwrite('Rotate.jpg', image3)


# Part 3
n = 3
image4, WxExpTemp, WyExpTemp = RewritePicture(image2, n, WxLn, WyLn, XMaxLn)
cv2.imwrite('repeat.jpg', image4)

# Part 4
newZmatrix = MakeComplexMatrix(WxExpTemp, WyExpTemp)
alpha = math.atan(math.log(r2 / r1) / 2 / math.pi)
WxExp1, WyExp1 = MakeMatricesMappedRotate(newZmatrix, alpha, row * n, column)
WxExp2, WyExp2 = MakeCoherenceW(WxExp1, WyExp1, math.cos(alpha))
WxExp3, WyExp3, xminR, xmaxR, yminR, ymaxR = MakeMatricesMappedExp(WxExp2, WyExp2)
WxExp4, WyExp4 = MakeMatriciesMappedS(WxExp3, WyExp3, xmaxR - xminR, -xminR, row, ymaxR - yminR, -yminR, column)
image5 = MakeWExpMappedMatrix(image1, image4, WxExp4, WyExp4, row, column)
cv2.imwrite('Exp.jpg', image5)


